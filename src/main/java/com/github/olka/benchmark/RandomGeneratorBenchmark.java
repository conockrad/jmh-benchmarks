package com.github.olka.benchmark;

import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;

import java.util.Random;
import java.util.SplittableRandom;
import java.util.concurrent.ThreadLocalRandom;

@State(Scope.Benchmark)
@Warmup(iterations = 2)
@Measurement(iterations = 5)
public class RandomGeneratorBenchmark {

	Random random = new Random();
	ThreadLocalRandom threadLocalRandom = ThreadLocalRandom.current();
	SplittableRandom splitableRandom = new SplittableRandom();

	@Benchmark
	public void random(Blackhole bh) {
		for (int i = 0; i < 1000_000; i++)
			bh.consume(random.nextInt(127));
	}


	@Benchmark
	public void threadLocalRandomWithBounds(Blackhole bh) {
		for (int i = 0; i < 1000_000; i++)
			bh.consume(threadLocalRandom.nextInt(127));
	}

	@Benchmark
	public void threadLocalRandom(Blackhole bh) {
		for (int i = 0; i < 1000_000; i++)
			bh.consume(threadLocalRandom.nextInt());
	}

	@Benchmark
	public void splittableRandom(Blackhole bh) {
		for (int i = 0; i < 1000_000; i++)
			bh.consume(splitableRandom.nextInt());
	}

	@Benchmark
	public void splittableRandomWithBounds(Blackhole bh) {
		for (int i = 0; i < 1000_000; i++)
			bh.consume(splitableRandom.nextInt(127));
	}
}
