package com.github.olka.benchmark;

import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

public class Starter {
    public static void main(String[] args) throws RunnerException {
        String regexp = ".*Benchmark.*";
        Options options = new OptionsBuilder().include(regexp).threads(1).forks(1).shouldFailOnError(true).build();
        new Runner(options).run();
    }
}
